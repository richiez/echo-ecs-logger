# echo-ecs-logger

Logging middleware for golang echo framework using logrus and elastic common schema


## Example
```go
package main

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	mw "gitlab.com/richiez/echo-ecs-logger"
	"gitlab.com/richiez/echo-ecs-logger/log"
)

func main() {

	e := echo.New()

    // add custom fields for all entries 
    // *** requires using log.Logger().Entry() when appending add'l fields ***
    fields := make(map[string]interface{})
	fields["tags"] = "test"

    // initialize logger with vars
	mw.InitLogger(&mw.LoggerConfig{
        // AppName used as service.name in json output
        AppName:     "SampleLogger",
        // When true, logs will print formatted json
        PrettyPrint: true,
        // Minimum log level to write to logs
        Level:       logrus.DebugLevel,
        // Include details from calling method
        ReportCaller: true,
        // Static fields to use on all log entries
        StaticFields: fields,
	})

    // set middleware
	e.Use(mw.Logger())

	log.Logger().WithFields(logrus.Fields{
		"custom_field": "something",
	}).Infof("event called with custom field")

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	e.Logger.Fatal(e.Start(":3000"))
}

```

Calling the logger with custom fields added
```go
log.Logger().WithFields(logrus.Fields{
    "custom_field": "something",
}).Info("event called with custom field")
```

Sample output from when calling logger
```json
{
  "@timestamp": "2021-06-26T19:01:01.373-0400",
  "custom_field": "item occurred",
  "ecs.version": "1.6.0",
  "log.level": "info",
  "log.origin.file.line": 68,
  "log.origin.file.name": "/mnt/c/Users/rich/source/repos/cookbook/go/echo/main.go",
  "log.origin.function": "main.main",
  "message": "foo bar logged with custom field",
  "service.name": "foo",
  "tags": "test"
}
```


Sample output from Echo HTTP requests
```json
{
  "@timestamp": "2021-06-26T18:56:49.500-0400",
  "destination.address": "/",
  "ecs.version": "1.6.0",
  "http.request.bytes": "0",
  "http.request.id": "",
  "http.request.method": "GET",
  "http.request.referrer": "",
  "http.request.status_code": 200,
  "log.level": "info",
  "log.origin.file.line": 83,
  "log.origin.file.name": "/mnt/c/Users/rich/source/repos/cookbook/go/echo-ecs-logger/ecs_middleware.go",
  "log.origin.function": "gitlab.com/cookbook/go/echo-ecs-logger.Logger.func1.1",
  "message": "localhost:3000 200 9.6µs",
  "service.name": "foo",
  "tags": "test",
  "url.domain": "localhost:3000",
  "user_agent.original": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"
}

```

## Referenced
- https://github.com/labstack/echo
- https://github.com/neko-neko/echo-logrus
- https://github.com/elastic/ecs-logging-go-logrus
- https://www.elastic.co/guide/en/ecs/current/ecs-field-reference.html