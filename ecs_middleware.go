package ecsmiddleware

import (
	"os"
	"time"

	"gitlab.com/richiez/echo-ecs-logger/log"
	"go.elastic.co/ecslogrus"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

var (
	appName       string = ""
	DefaultLogger        = &LoggerConfig{
		AppName:      "AppName",
		PrettyPrint:  true,
		Level:        logrus.DebugLevel,
		ReportCaller: true,
	}
)

type LoggerConfig struct {
	Level        logrus.Level
	AppName      string
	ReportCaller bool
	PrettyPrint  bool
	StaticFields map[string]interface{}
}

func InitLogger(c *LoggerConfig) {
	log.Logger().SetOutput(os.Stdout)
	log.Logger().SetLevel(c.Level)

	log.Logger().SetFormatter(&ecslogrus.Formatter{
		PrettyPrint: c.PrettyPrint,
	})
	log.Logger().SetReportCaller(c.ReportCaller)

	appName = c.AppName

	log.Logger().SetAppName(appName)
	log.Logger().SetStaticFields(c.StaticFields)

}

func Logger() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {

			req := c.Request()
			res := c.Response()
			start := time.Now()

			var err error
			if err = next(c); err != nil {
				c.Error(err)
			}
			stop := time.Now()

			id := req.Header.Get(echo.HeaderXRequestID)
			if id == "" {
				id = res.Header().Get(echo.HeaderXRequestID)
			}
			reqSize := req.Header.Get(echo.HeaderContentLength)
			if reqSize == "" {
				reqSize = "0"
			}

			log.Logger().Entry().WithFields(
				logrus.Fields{
					"service":     logrus.Fields{"name": appName},
					"user_agent":  logrus.Fields{"original": req.UserAgent()},
					"destination": logrus.Fields{"address": req.RequestURI},
					"url":         logrus.Fields{"domain": req.Host},
					"http": logrus.Fields{
						"request": logrus.Fields{
							"method":      req.Method,
							"id":          id,
							"referrer":    req.Referer(),
							"status_code": res.Status,
							"bytes":       reqSize,
						},
					},

					// "service.name":             appName,
					// "http.request.method":      req.Method,
					// "http.request.id":          id,
					// "http.request.referrer":    req.Referer(),
					// "http.request.status_code": res.Status,
					// "http.request.bytes":       reqSize,
					// "url.domain":               req.Host,
					// "user_agent.original":      req.UserAgent(),
					// "destination.address":      req.RequestURI,
				},
			).Infof("%s %3d %s", req.Host, res.Status, stop.Sub(start).String())

			// log.Logger().Entry().WithField(
			// 	"service", logrus.Fields{"name": appName},
			// ).WithField("user_agent", logrus.Fields{"original": req.UserAgent()}).WithField("destination", logrus.Fields{"address": req.RequestURI}).WithField("url", logrus.Fields{"domain": req.Host}).Infof("%s %3d %s", req.Host, res.Status, stop.Sub(start).String())

			return err
		}
	}
}
