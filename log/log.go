package log

import (
	"encoding/json"
	"io"

	"github.com/labstack/gommon/log"
	"github.com/sirupsen/logrus"
)

// EcsLogger extend logrus.Logger
type EcsLogger struct {
	*logrus.Logger
}

// Singleton logger
var (
	singletonLogger = &EcsLogger{
		Logger: logrus.New(),
	}
	appName      = ""
	staticFields map[string]interface{}
)

// Logger return singleton logger
func Logger() *EcsLogger {
	return singletonLogger
}

func (el *EcsLogger) Entry() *logrus.Entry {
	//return Logger().WithField("service.name", appName)
	return Logger().baseEntry()
}

// Print output message of print level
func Print(i ...interface{}) {
	singletonLogger.Print(i...)
}

// Printf output format message of print level
func Printf(format string, i ...interface{}) {
	singletonLogger.Printf(format, i...)
}

// Printj output json of print level
func Printj(j log.JSON) {
	singletonLogger.Printj(j)
}

// Debug output message of debug level
func Debug(i ...interface{}) {
	singletonLogger.Debug(i...)
}

// Debugf output format message of debug level
func Debugf(format string, args ...interface{}) {
	singletonLogger.Debugf(format, args...)
}

// Debugj output json of debug level
func Debugj(j log.JSON) {
	singletonLogger.Debugj(j)
}

// Info output message of info level
func Info(i ...interface{}) {
	singletonLogger.Info(i...)
}

// Infof output format message of info level
func Infof(format string, args ...interface{}) {
	singletonLogger.Infof(format, args...)
}

// Infoj output json of info level
func Infoj(j log.JSON) {
	singletonLogger.Infoj(j)
}

// Warn output message of warn level
func Warn(i ...interface{}) {
	singletonLogger.Warn(i...)
}

// Warnf output format message of warn level
func Warnf(format string, args ...interface{}) {
	singletonLogger.Warnf(format, args...)
}

// Warnj output json of warn level
func Warnj(j log.JSON) {
	singletonLogger.Warnj(j)
}

// Error output message of error level
func Error(i ...interface{}) {
	singletonLogger.Error(i...)
}

// Errorf output format message of error level
func Errorf(format string, args ...interface{}) {
	singletonLogger.Errorf(format, args...)
}

// Errorj output json of error level
func Errorj(j log.JSON) {
	singletonLogger.Errorj(j)
}

// Fatal output message of fatal level
func Fatal(i ...interface{}) {
	singletonLogger.Fatal(i...)
}

// Fatalf output format message of fatal level
func Fatalf(format string, args ...interface{}) {
	singletonLogger.Fatalf(format, args...)
}

// Fatalj output json of fatal level
func Fatalj(j log.JSON) {
	singletonLogger.Fatalj(j)
}

// Panic output message of panic level
func Panic(i ...interface{}) {
	singletonLogger.Panic(i...)
}

// Panicf output format message of panic level
func Panicf(format string, args ...interface{}) {
	singletonLogger.Panicf(format, args...)
}

// Panicj output json of panic level
func Panicj(j log.JSON) {
	singletonLogger.Panicj(j)
}

// To logrus.Level
func toLogrusLevel(level log.Lvl) logrus.Level {
	switch level {
	case log.DEBUG:
		return logrus.DebugLevel
	case log.INFO:
		return logrus.InfoLevel
	case log.WARN:
		return logrus.WarnLevel
	case log.ERROR:
		return logrus.ErrorLevel
	}

	return logrus.InfoLevel
}

// To Echo.log.lvl
func toEchoLevel(level logrus.Level) log.Lvl {
	switch level {
	case logrus.DebugLevel:
		return log.DEBUG
	case logrus.InfoLevel:
		return log.INFO
	case logrus.WarnLevel:
		return log.WARN
	case logrus.ErrorLevel:
		return log.ERROR
	}

	return log.OFF
}

// Output return logger io.Writer
func (l *EcsLogger) Output() io.Writer {
	return l.Out
}

// SetOutput logger io.Writer
func (l *EcsLogger) SetOutput(w io.Writer) {
	l.Out = w
}

// Level return logger level
func (l *EcsLogger) Level() log.Lvl {
	return toEchoLevel(l.Logger.Level)
}

// SetLevel logger level
func (l *EcsLogger) SetLevel(v logrus.Level) {
	l.Logger.Level = v
}

// SetHeader logger header
// Managed by Logrus itself
// This function do nothing
func (l *EcsLogger) SetHeader(h string) {
	// do nothing
}

// Formatter return logger formatter
func (l *EcsLogger) Formatter() logrus.Formatter {
	return l.Logger.Formatter
}

// SetFormatter logger formatter
// Only support logrus formatter
func (l *EcsLogger) SetFormatter(formatter logrus.Formatter) {
	l.Logger.Formatter = formatter
}

// Prefix return logger prefix
// This function do nothing
func (l *EcsLogger) Prefix() string {
	return ""
}

// SetPrefix logger prefix
// This function do nothing
func (l *EcsLogger) SetPrefix(p string) {
	// do nothing
}

// Print output message of print level
func (l *EcsLogger) Print(i ...interface{}) {
	l.Logger.Print(i...)
}

// Printf output format message of print level
func (l *EcsLogger) Printf(format string, args ...interface{}) {
	l.Logger.Printf(format, args...)
}

// Printj output json of print level
func (l *EcsLogger) Printj(j log.JSON) {
	b, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	l.Logger.Println(string(b))
}

// Debug output message of debug level
func (l *EcsLogger) Debug(i ...interface{}) {
	l.baseEntry().Debug(i...)
}

// Debugf output format message of debug level
func (l *EcsLogger) Debugf(format string, args ...interface{}) {
	l.baseEntry().Debugf(format, args...)
}

// Debugj output message of debug level
func (l *EcsLogger) Debugj(j log.JSON) {
	b, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	l.baseEntry().Debugln(string(b))
}

// Info output message of info level
func (l *EcsLogger) Info(i ...interface{}) {
	l.baseEntry().Info(i...)
}

// Infof output format message of info level
func (l *EcsLogger) Infof(format string, args ...interface{}) {
	l.baseEntry().Infof(format, args...)
}

// Infoj output json of info level
func (l *EcsLogger) Infoj(j log.JSON) {
	b, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	l.baseEntry().Infoln(string(b))
}

// Warn output message of warn level
func (l *EcsLogger) Warn(i ...interface{}) {
	l.baseEntry().Warn(i...)
}

// Warnf output format message of warn level
func (l *EcsLogger) Warnf(format string, args ...interface{}) {
	l.baseEntry().Warnf(format, args...)
}

// Warnj output json of warn level
func (l *EcsLogger) Warnj(j log.JSON) {
	b, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	l.baseEntry().Warnln(string(b))
}

// Error output message of error level
func (l *EcsLogger) Error(i ...interface{}) {
	l.baseEntry().Error(i...)
}

// Errorf output format message of error level
func (l *EcsLogger) Errorf(format string, args ...interface{}) {
	l.baseEntry().Errorf(format, args...)
}

// Errorj output json of error level
func (l *EcsLogger) Errorj(j log.JSON) {
	b, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	l.baseEntry().Errorln(string(b))
}

// Fatal output message of fatal level
func (l *EcsLogger) Fatal(i ...interface{}) {
	l.baseEntry().Fatal(i...)
}

// Fatalf output format message of fatal level
func (l *EcsLogger) Fatalf(format string, args ...interface{}) {
	l.baseEntry().Fatalf(format, args...)
}

// Fatalj output json of fatal level
func (l *EcsLogger) Fatalj(j log.JSON) {
	b, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	l.baseEntry().Fatalln(string(b))
}

// Panic output message of panic level
func (l *EcsLogger) Panic(i ...interface{}) {
	l.baseEntry().Panic(i...)
}

// Panicf output format message of panic level
func (l *EcsLogger) Panicf(format string, args ...interface{}) {
	l.baseEntry().Panicf(format, args...)
}

// Panicj output json of panic level
func (l *EcsLogger) Panicj(j log.JSON) {
	b, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	l.baseEntry().Panicln(string(b))
}

// SetAppName service.name for logs
func (l *EcsLogger) SetAppName(name string) {
	appName = name
}

// SetStaticFields fields to include with all logs
func (l *EcsLogger) SetStaticFields(fields map[string]interface{}) {
	staticFields = fields
}

// baseEntry to enter static fields and service name
func (l *EcsLogger) baseEntry() *logrus.Entry {
	if appName == "" {
		return l.Logger.WithFields(staticFields)
	} else {
		return l.Logger.WithFields(staticFields).WithField("service", logrus.Fields{"name": appName})
	}
}
