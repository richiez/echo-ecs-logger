module gitlab.com/richiez/echo-ecs-logger

go 1.16

require (
	github.com/labstack/echo/v4 v4.3.0
	github.com/labstack/gommon v0.3.0
	github.com/sirupsen/logrus v1.8.1
	go.elastic.co/ecslogrus v1.0.0
)
